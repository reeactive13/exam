package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.isEmpty() || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }
        double h = ((-1 + Math.sqrt(1.0 + 8.0 * inputNumbers.size())) / 2);
        if (h % 1 != 0) {
            throw new CannotBuildPyramidException();
        }
        int height = (int) h;
        int foundation = height * 2 - 1;
        int[][] array = new int[height][foundation];
        Collections.sort(inputNumbers);
        int centerPoint = foundation / 2;
        int digitsOnLine = 1;
        int listIndex = 0;
        for (int i = 0, delta = 0; i < height; i++, delta++, digitsOnLine++) {
            int firstDigit = centerPoint - delta;
            for (int j = 0; j < digitsOnLine * 2; j += 2, listIndex++) {
                array[i][firstDigit + j] = inputNumbers.get(listIndex);
            }
        }
        /* Optional array print
        for (int[] a : array) {
            for (int b : a)
                System.out.print(b + " ");
            System.out.println();
        }*/
        return array;
    }
}
