package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (!checkSymbols(statement)) {
            return null;
        }
        LinkedList<Double> st = new LinkedList<Double>();
        LinkedList<Character> op = new LinkedList<Character>();
        for (int i = 0; i < statement.length(); i++) {
            char c = statement.charAt(i);
            if (c == '(')
                op.add('(');
            else if (c == ')') {
                while (op.getLast() != '(')
                    processOperator(st, op.removeLast());
                op.removeLast();
            } else if (isOperator(c)) {
                while (!op.isEmpty() && priority(op.getLast()) >= priority(c))
                    processOperator(st, op.removeLast());
                op.add(c);
            } else {
                String operand = "";
                while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.'))
                    operand += statement.charAt(i++);
                --i;
                st.add(Double.parseDouble(operand));
            }
        }
        while (!op.isEmpty())
            processOperator(st, op.removeLast());

        Double result = st.get(0);
        if (result.isInfinite()) {
            return null;
        }
        if (result % 1 == 0) {
            return String.valueOf(result.intValue());
        }
        return result.toString();
    }

    private boolean checkSymbols(String s) {
        int checkParentheses = 0;
        if (s == null || s.length() < 1 || isOperator(s.charAt(s.length() - 1))) {
            return false;
        }
        for (int i = 0; i < s.length() - 1; i++) {
            if (isOperator(s.charAt(i)) || s.charAt(i) == '.') {
                if (isOperator(s.charAt(i + 1)) || (s.charAt(i + 1) == '.')) {
                    return false;
                }
            }
            if (s.charAt(i) == ','){
                return false;
            }
            if (s.charAt(i) == '(') {
                checkParentheses++;
            }
            if (s.charAt(i) == ')') {
                checkParentheses--;
            }
        }
        return checkParentheses == 0;
    }

    private boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '%';
    }

    private int priority(char op) {
        switch (op) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
            case '%':
                return 2;
            default:
                return -1;
        }
    }

    private void processOperator(LinkedList<Double> st, char op) {
        double r = st.removeLast();
        double l = st.removeLast();
        switch (op) {
            case '+':
                st.add(l + r);
                break;
            case '-':
                st.add(l - r);
                break;
            case '*':
                st.add(l * r);
                break;
            case '/':
                st.add(l / r);
                break;
            case '%':
                st.add(l % r);
                break;
        }
    }
}
